import { gamesJson } from "./db/db.js";
search(gamesJson)

document.getElementById("buttonclick").addEventListener("click", function() {
    search(gamesJson);
});

// Below function Executes on click of login button.
var input = document.getElementById("total_time");
input.addEventListener("keypress", function(event) {
    if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("buttonclick").click();
    }
});

function search(data){
    //delete all the previous displayed data in the html
    var div = document.getElementById("myData");
    while(div.firstChild){
        div.removeChild(div.firstChild);
    }


    //make an array of the db.js file wich is actually JSON
    var jsonData = JSON.parse(data)

    let newArray = jsonData;

    var formid = document.getElementById("id").value;

    var name = document.getElementById("username").value;
    var total_time = document.getElementById("total_time").value;
    var winOrLose = document.getElementById("winOrLose").value;
    var score = document.getElementById("score").value;

    if (formid !== ""){
        newArray = newArray.filter(function (item) {
            return item["game_id"] == formid
        })


    }

        if (formid === "" && winOrLose !== "") {

            newArray = newArray.filter(function (item) {
                console.log(winOrLose)
                return item["winOrLose"] == winOrLose
            })


        }

    if (formid === "" &&  score !== "") {



        if (score == 250){
            newArray = newArray.filter(function (item) {
                return item["score"] >= 0 && item["score"] <= score
            })
        }


        if (score == 500){
            newArray = newArray.filter(function (item) {
                return item["score"] >= 250 && item["score"] <= score
            })
        }


        if (score == 1000){
            newArray = newArray.filter(function (item) {
                return item["score"] >= 500 && item["score"] <= score
            })
        }

    else {
            newArray = newArray.filter(function (item) {
                return item["score"] >= 0
            })

        }




    }

    if (formid === "" &&  name !== "") {

        newArray = newArray.filter(function (item) {
            return item["player_name"] == name
        })



    }




    CreateTableFromJSON(newArray)
}


function appendData(data) {
    var mainContainer = document.getElementById("myData");
    for (var i = 0; i < data.length; i++) {
        var div = document.createElement("div");
        div.innerHTML = 'Name: ' + data[i].player_name + ' gameid: ' + data[i].game_id;
        mainContainer.appendChild(div);
    }

    if (data.length === 0){
        var div = document.createElement("div");
        div.innerHTML = 'No results found';
        mainContainer.appendChild(div);
    }
}
function CreateTableFromJSON(data) {

    var sortMessage = ' sort [v]'


    var headers = [];
    for (var i = 0; i < data.length; i++) {
        for (var key in data[i]) {
            if (headers.indexOf(key) === -1) {
                headers.push(key);
            }
        }
    }

    var CreateTable = document.createElement("table");
    var tr = CreateTable.insertRow(-1);

    for (var i = 0; i < headers.length; i++) {
        var th = document.createElement("th");
        console.log(headers[i])
        if (headers[i] == "score"){

            th.onclick = function () {

                console.log(sorted(data))
                if (!sorted(data)) {
                    var newData = data.sort(function (a, b) {
                        return a.score - b.score;
                    });
                }

                else {
                    if (sorted(data)) {
                        var newData = data.sort(function (a, b) {
                            return b.score - a.score;
                        });
                    }
                }
                CreateTableFromJSON(newData)
            };



            if(sorted(data)){
                sortMessage = sortMessage.replace('[v]','[^]')
            }

            th.innerHTML = headers[i] +  sortMessage;

        }
        else if (headers[i] == "game_id"){

            th.onclick = function () {

                console.log(data)



                if (!sorted2(data)) {

                    var newData = data.sort(function (a, b) {

                        return a['game_id'] - b['game_id'];

                    });

                }

                else {
                    if (sorted2(data)) {
                        var newData = data.sort(function (a, b) {
                            return b['game_id'] - a['game_id'];
                        });
                    }
                }


                console.log(sorted2(newData))


                console.log(newData)
                CreateTableFromJSON(newData)


            };
            th.innerHTML = headers[i] +  ' sort [v]';

        }
        else {
            th.innerHTML = headers[i];
        }

        tr.appendChild(th);
    }

    for (var i = 0; i < data.length; i++) {

        tr = CreateTable.insertRow(-1);

        for (var j = 0; j < headers.length; j++) {
            var cells = tr.insertCell(-1);
            cells.innerHTML = data[i][headers[j]];
        }
    }

    var container = document.getElementById("myData");

    if (data.length === 0){
        var div = document.createElement("div");
        div.innerHTML = 'No results found';
        container.appendChild(div);
    }
    else {

        container.innerHTML = "";
        container.appendChild(CreateTable);
    }
}


function sorted(data){
    let b;
    for(let a = 0; a < data.length-1; a++){
        b = a + 1;
            if (data[b].score - data[a].score < 0) return false;
    }
    return true;
}
function sorted2(data){
    let b;
    for(let a = 0; a < data.length-1; a++){
        b = a + 1;
        if (data[b]['game_id'] - data[a]['game_id'] < 0) return false;
    }
    return true;
}
