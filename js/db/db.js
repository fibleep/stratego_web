export const gamesJson = `[
    {
        "game_id": 57,
        "player_name": "dwa",
        "total_time": null,
        "winOrLose": "win",
        "score": 900
    },
    {
        "game_id": 98,
        "player_name": "qwe",
        "total_time": null,
        "winOrLose": "lose",
        "score": 539
    },
    {
        "game_id": 102,
        "player_name": "dwa",
        "total_time": null,
        "winOrLose": "win",
        "score": 50
    },
    {
        "game_id": 125,
        "player_name": "WAE",
        "total_time": null,
        "winOrLose": "win",
        "score": 501
    },
    {
        "game_id": 5,
        "player_name": "qwe1",
        "total_time": null,
        "winOrLose": "win",
        "score": 599
    },
    {
        "game_id": 8,
        "player_name": "MONKEY",
        "total_time": null,
        "winOrLose": "win",
        "score": 50
    },
    {
        "game_id": 62,
        "player_name": "dq",
        "total_time": null,
        "winOrLose": "win",
        "score": 1245
    },
    {
        "game_id": 114,
        "player_name": "Gilles",
        "total_time": null,
        "winOrLose": "win",
        "score": 799
    },
    {
        "game_id": 51,
        "player_name": "qwer",
        "total_time": null,
        "winOrLose": "win",
        "score": 50
    },
    {
        "game_id": 59,
        "player_name": "dwa",
        "total_time": null,
        "winOrLose": "win",
        "score": 600
    },
    {
        "game_id": 6,
        "player_name": "Swag",
        "total_time": null,
        "winOrLose": "win",
        "score": 550
    },
    {
        "game_id": 4,
        "player_name": "qwe",
        "total_time": null,
        "winOrLose": "win",
        "score": 400
    },
    {
        "game_id": 53,
        "player_name": "qwe",
        "total_time": null,
        "winOrLose": "lose",
        "score": 350
    },
    {
        "game_id": 49,
        "player_name": "qwe",
        "total_time": null,
        "winOrLose": "lose",
        "score": 300
    },
    {
        "game_id": 124,
        "player_name": "WAE",
        "total_time": null,
        "winOrLose": "win",
        "score": 200
    },
    {
        "game_id": 9,
        "player_name": "New",
        "total_time": null,
        "winOrLose": "lose",
        "score": 150
    },
     {
        "game_id": 9,
        "player_name": "New",
        "total_time": null,
        "winOrLose": "lose",
        "score": 100
    }
]`
